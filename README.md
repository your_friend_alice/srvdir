# srvdir

Regular old HTTP file server, except directory listings are in JSON.

## Usage

```
  -address string
        Address to listen on (default "0.0.0.0:8080")
  -base string
        Base directory to serve (default ".")
  -hidden
        Show hidden files
```

## JSON Format

```
{
    "files": [
        "<file_name>": {
            "modified": "<ISO8601_formatted_modification_time>",
            "size": <size_in_bytes>
        },
        ...
    ],
    "dirs": [
        "<dir_name>": {
            "modified": "<ISO8601_formatted_modification_time>"
        },
        ...
    ]
}
```

## Headers

If you're writing an application to consume this API, you need to tell if you're getting a directory listening or a file! Checking the `Content-Type` header for `application/json` isn't reliable since the server may just be pointed at a JSON file. Here's unambiguous ways:

- Directory listings will have an HTTP status of 300
- Files will have a `Content-Disposition` header, setting the filename to the basename of the file being served.
