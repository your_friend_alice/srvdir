package main

import (
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
	"path/filepath"
	"strings"
)

func main() {
	address := flag.String("address", "0.0.0.0:8080", "Address to listen on")
	base := flag.String("base", ".", "Base directory to serve")
	hidden := flag.Bool("hidden", false, "Show hidden files")
	flag.Parse()
	if flag.NArg() > 0 {
		flag.Usage()
		os.Exit(2)
	}

	err := os.Chdir(*base)
	if err != nil {
		log.Fatal(err.Error())
	}

	http.Handle("/", filesystemHandler{ShowHidden: *hidden})
	log.Fatal(http.ListenAndServe(*address, nil))
}

type Dir struct {
	Modified time.Time `json:"modified"`
}

type File struct {
	Modified time.Time `json:"modified"`
	Size int64 `json:"size"`
}

type Listing struct {
	Files map[string]File `json:"files"`
	Dirs  map[string]Dir `json:"dirs"`
}

type ErrorResponse struct {
	ErrorCode    int    `json:"errorCode"`
	ErrorMessage string `json:"errorMessage"`
}

func fail(res http.ResponseWriter, status int, message string) {
	contentType(res, "application/json")
	res.WriteHeader(status)
	e := json.NewEncoder(io.MultiWriter(res, log.Writer()))
	err := e.Encode(ErrorResponse{status, message})
	if err != nil {
		log.Println(err.Error())
	}
}

func handleOsError(res http.ResponseWriter, err error) {
	if os.IsPermission(err) {
		fail(res, 403, err.Error())
	} else if os.IsNotExist(err) {
		fail(res, 404, err.Error())
	} else {
		fail(res, 500, err.Error())
	}
}

func contentType(res http.ResponseWriter, t string) {
	res.Header().Set("Content-Type", t)
}

type filesystemHandler struct {
	ShowHidden bool
}

func (h filesystemHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" && req.Method != "HEAD" {
		fail(res, 400, "Allowed methods: GET, HEAD")
		return
	}
	path := filepath.Join(".", filepath.FromSlash(req.URL.Path))
	info, err := os.Lstat(path)
	if err != nil {
		handleOsError(res, err)
		return
	}
	if info.IsDir() {
		ls(res, path, h.ShowHidden)
	} else if info.Mode().IsRegular() {
		res.Header().Set("Content-Disposition", "filename=" + filepath.Base(path))
		http.ServeFile(res, req, path)
	}
}

func ls(res http.ResponseWriter, path string, hidden bool) {
	contentType(res, "application/json")
	info, err := os.Lstat(path)
	if err != nil {
		handleOsError(res, err)
		return
	}
	res.Header().Set("Last-Modified", info.ModTime().UTC().Format(http.TimeFormat))
	files, err := ioutil.ReadDir(path)
	if err != nil {
		handleOsError(res, err)
		return
	}
	l := Listing{
		Files: map[string]File{},
		Dirs: map[string]Dir{},
	}
	for _, file := range files {
		if strings.HasPrefix(file.Name(), ".") {
			continue
		}
		if file.IsDir() {
			l.Dirs[file.Name()] = Dir{Modified: file.ModTime()}
		} else if file.Mode().IsRegular() {
			l.Files[file.Name()] = File{Modified: file.ModTime(), Size: file.Size()}
		}
	}
	res.WriteHeader(300)
	e := json.NewEncoder(res)
	err = e.Encode(l)
	if err != nil {
		log.Println(err.Error())
	}
}
